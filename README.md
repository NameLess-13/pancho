# Project Title

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Contributing](../CONTRIBUTING.md)

## About <a name = "about"></a>

Proyecto, casa inteligente, capaz de mandar instrucciones a un arduino maestro con varios clientes
simultaneos a un solo puerto serie, el arduino maestro puede conectar co diferentes esclavos
por medio de I2C

## Getting Started <a name = "getting_started"></a>

Se usa como una liberia cualquiera de python

### Prerequisites

* Python 3
* PySerial 3.5
* Thread

### Installing

python3 insatall setup.py

## Usage <a name = "usage"></a>

las instrucciones van de acuerdo:


ejemplo de uso, hacer parpadear un led:

while (count < 15):
    count+=1
    if (count % 3 == 0):
        send("high")
    else:
        send("low")

send(DISCONNECT_MESSAGE)
