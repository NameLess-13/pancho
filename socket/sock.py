#!/usr/bin/env python3
import socket
import threading

import serial
from collections import deque
from python_arduino import sendComand

#! Configuracion base de socket
HEADER = 64
PORT = 5050
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"

#! preparamos conexion de servidor
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)

#! configurcion de conexion serire con arduino
arduino = serial.Serial('/dev/ttyACM0', 115200)
#! instancia de variables para hacer la cola de peticiones
peticiones = deque()
respuestas = []

def atender():
    while True:
        if len(peticiones) > 0:
            peticion = peticiones.popleft()
            respuestas.append(str(peticion)+":"+str(sendComand(peticion)))

def handle_client(conn, addr):
    print("[NEW CONNECTION] "+str(addr)+" connected.")

    connected = True
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            if msg == DISCONNECT_MESSAGE:
                connected = False

            print("["+str(addr)+"] "+str(msg))
            if msg != DISCONNECT_MESSAGE:
                peticiones.append(msg)
                while True:
                    response = getResponse(msg)
                    if (response != "404"):
                        arduinoResponse = "Arduino reponse: "+str(response)
                        conn.send(arduinoResponse.encode(FORMAT))
                        break

    conn.close()

def getResponse(msg):
    response = "404"
    rp_tmp = []
    for index,item in enumerate(respuestas):
        rp_tmp = item.split(":")
        if rp_tmp[0] == msg:
            response = rp_tmp[1]
            respuestas.pop(index)
            break
    return response

def start():
    server.listen()
    print("[LISTENING] Server is listening on "+str(SERVER))
    thread_cola = threading.Thread(target=atender)
    thread_cola.start()
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print("[ACTIVE CONNECTIONS] "+ str(threading.activeCount() - 2))


print("[STARTING] server is starting...")
start()
